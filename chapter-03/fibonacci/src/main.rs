// Note: recursion slow down significantly around pos = 40
// fn calculate_fib_r(pos: usize) -> u64 {
//     match pos {
//         1 => 0,
//         2 => 1,
//         x => calculate_fib_r(x - 1) + calculate_fib_r(x - 2),
//     }
// }

fn calculate_fib_i(mut pos: usize) -> u64 {
    let mut last = [0, 1];

    while 0 < pos {
        last = [last[1], last[0] + last[1]];

        pos -= 1;
    }

    last[pos]
}

fn main() {
    for x in 0..51 {
        let fib = calculate_fib_i(x);

        println!("The {} number in the Fibonacci sequence is {}", x, fib);
    }
}
