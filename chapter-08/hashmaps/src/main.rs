use std::collections::HashMap;

fn create_hashmap() {
    let mut scores = HashMap::new();

    // NOTE: all keys must have the same type (so are the values)
    // scores.insert("Blue", 10);
    // scores.insert("Yellow", 50);

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    println!("{:?}", scores);
}

fn create_using_itarables() {
    let teams = vec!["Red", "Blue"];
    let initial_scores = vec![20, 45];
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    println!("{:?}", scores);
}

fn get_value_from_hashmap() {
    let mut scores = HashMap::new();

    scores.insert("Blue", 10);
    scores.insert("Yellow", 50);

    println!("{:?}", scores.get("Blue"));
    println!("{:?}", scores.get("Red"));

    for (k, v) in &scores {
        println!("{}: {}", k, v);
    }
}

fn update_hashmap() {
    let mut scores = HashMap::new();

    // put value for key overwriting the existing value.
    scores.insert("Blue", 10);
    scores.insert("Blue", 34);

    // put value in key only if there's no existing value
    scores.entry("Blue").or_insert(50);
    scores.entry("Yellow").or_insert(50);

    println!("{:?}", scores.get("Blue"));
    println!("{:?}", scores.get("Red"));

    for (k, v) in &scores {
        println!("{}: {}", k, v);
    }

    // NOTE: Score is borrowed mutably in following line.
    let blue = scores.entry("Blue").or_insert(0);
    *blue += 1;

    println!("{:?}", blue);
}

fn main() {
    create_hashmap();
    create_using_itarables();
    get_value_from_hashmap();
    update_hashmap();
}
