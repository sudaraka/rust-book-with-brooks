fn pushing() {
    let mut s1 = String::from("one");
    let s2 = "three";
    let s3 = String::from("four");
    let end = '.';

    s1.push_str("two");
    s1.push_str(s2);

    // We can NOT move a `String` to `push_str`
    // s1.push_str(s3);

    // Sending a reference (`&String`) or let `push_str` borrow it is allowed.
    s1.push_str(&s3);

    // NOTE using `push` instead of `push_str` for character type.
    s1.push(end);

    println!("s1 is {}", s1);
    println!("s2 is {}", s2);
    println!("s3 is {}", s3);
    println!("end is {}", end);
}

fn concatanating() {
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");

    let s3 = s1 + &s2;

    // Following use of `s1` will cause a "use after move" error because the
    // concatenation operator(?) `+` takes the ownership of `s1`.
    // And we can't pass `&String` as a left operand of the `+` operator.
    // println!("s1 is {}", s1);

    println!("s2 is {}", s2);
    println!("s3 is {}", s3);

    let mut s4 = String::from("and Hello, ");
    let s5 = String::from("another world!");

    // `+=` operator solved the issue we mentioned above, but we have to make
    // the left operand mutable.
    s4 += &s5;

    println!("s4 is {}", s4);
    println!("s5 is {}", s5);
}

fn format() {
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");

    // NOTE: unlike in `+` operator used in `concatanating`, `format!` macro can
    // use `String` type with out taking ownership.
    let s3 = format!("{}{}", s1, s2);

    println!("s1 is {}", s1);
    println!("s2 is {}", s2);
    println!("s3 is {}", s3);
}

fn main() {
    pushing();
    concatanating();
    format();
}
