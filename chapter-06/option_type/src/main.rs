fn main() {
    let op1: Option<u32> = Some(42);
    let op2: Option<u32> = None;
    let op3: Option<String> = None;

    println!("{:?}", op1);
    println!("{:?}", op2);
    println!("{:?}", op3);

    let mut op4: Option<String> = Some("Hello".into());

    // Following generate an error because we have defined `op4` as
    // `Option<String>` and the value given is an integer.
    // op4 = Some(123);

    println!("{:?}", op4);

    let n1: Option<u32> = Some(42);
    let n2: Option<u32> = Some(2);

    // Following generate and error because values of `Option<T>` type can not
    // be used for `T` type oerations.
    // println!("Sum is {:?}", n1 + n2);
}
