#[derive(Debug)]
enum OS {
    Windows,
    Mac,
    Linux,
    Other(String),
}

impl OS {
    fn something(name: &str) -> OS {
        OS::Other(String::from(name))
    }
}

fn main() {
    let os1 = OS::something("Amiga");
    let os2 = OS::Windows;
    let os3 = OS::Mac;
    let os4 = OS::Linux;

    println!("{:?}", os1);
    println!("{:?}", os2);
    println!("{:?}", os3);
    println!("{:?}", os4);
}
