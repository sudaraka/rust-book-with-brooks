fn longest<'l1>(x: &'l1 str, y: &'l1 str) -> &'l1 str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn main() {
    let s1 = String::from("abcd");
    let result;

    {
        let s2 = String::from("xyz");
        result = longest(s1.as_str(), s2.as_str());
    }

    println!("The longest string is {}", result);
}
