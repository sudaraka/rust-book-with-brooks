static GREETING: &str = "Hello, world!";

static mut COUNTER: u32 = 0;

fn update_counter(value: u32) {
    unsafe {
        COUNTER += value;
    }
}

fn main() {
    println!("{}", GREETING);

    update_counter(2);

    unsafe {
        println!("Count: {}", COUNTER);

        update_counter(3);
        println!("Count: {}", COUNTER);
    }
}
