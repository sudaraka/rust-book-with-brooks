/// Add one to given number & return the result.
///
/// # Examples
///
/// ```
/// let value = 3;
/// assert_eq!(4, test_crate_1::add_one(value));
/// ````
pub fn add_one(x: i32) -> i32 {
    x + 11
}
