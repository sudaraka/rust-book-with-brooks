// NOTE: also see `chapter-02/guessing_game`

extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

mod types;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Please input your guess.");

        let mut raw_input = String::new();

        io::stdin()
            .read_line(&mut raw_input)
            .expect("Failed to read line");

        let guess = match raw_input.trim().parse() {
            Ok(num) => match types::Guess::new(num) {
                Ok(guess) => guess,
                Err(msg) => {
                    println!("{}", msg);

                    continue;
                }
            },
            Err(_) => continue,
        };

        println!("You guessed: {}", guess.value());

        match guess.value().cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
