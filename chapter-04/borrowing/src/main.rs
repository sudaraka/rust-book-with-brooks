fn main() {
    let mut s = String::from("hello");

    // Can't have two mutable reference to same object
    // let r1 = &mut s;
    // let r2 = &mut s;

    // Following is ok, because `r1` mutable reference goes out-of-scope before
    // we obtain the `r2` mutable reference.
    // {
    //     let r1 = &mut s;
    // }
    // let r2 = &mut s;

    // May **immutable** references are OK.
    let r3 = &s;
    let r4 = &s;
    // But not **mutable references** even if previous references were immutable.
    let r5 = &mut s;
}
